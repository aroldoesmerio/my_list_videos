import 'package:my_movies_list/data/models/genre_model.dart';

abstract class GenreRepositoryInterface {
  Future<List<GenreModel>> getGenreList();
}
