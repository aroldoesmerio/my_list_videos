import 'package:flutter/material.dart';
import 'package:my_movies_list/data/models/title_model.dart';
import 'package:my_movies_list/data/repositories/title_repository.dart';
import 'package:my_movies_list/ui/widgets/title_thumbnail.dart';

class SearchPage extends StatelessWidget {
  static const name = 'search-page';
  // final _repository = TitleRepository();

  SearchPage({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return SafeArea(
      child: Scaffold(
        appBar: AppBar(
          title: TextFormField(
            style: const TextStyle(
              color: Colors.white,
            ),
            decoration: const InputDecoration(
              hintText: 'Pesquise aqui...',
              hintStyle: TextStyle(color: Colors.white30),
              border: InputBorder.none,
            ),
          ),
        ),
        body: GridView.count(
          padding: const EdgeInsets.all(5.0),
          mainAxisSpacing: 5.0,
          crossAxisCount: 3,
          children: [],
          // _repository.getTitleList().map((e) => _buildTitleCard(e)).toList(),
        ),
      ),
    );
  }

  Widget _buildTitleCard(TitleModel title) {
    return Center(
      child: TitleThumbnail(
        width: 80.0,
        height: 130.0,
        titleName: title.name,
        urlThumbnail: title.posterUrl,
      ),
    );
  }
}
