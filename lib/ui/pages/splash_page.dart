import 'package:flutter/material.dart';
import 'package:my_movies_list/data/repositories/user_repository_interface.dart';
import 'package:my_movies_list/locator.dart';
import 'package:my_movies_list/ui/pages/home_page.dart';
import 'package:my_movies_list/ui/pages/login_page.dart';

class SplashPage extends StatefulWidget {
  static const name = 'splash-page';

  final _repository = getIt.get<UserRepositoryInterface>();

  SplashPage({Key? key}) : super(key: key);

  @override
  State<SplashPage> createState() => _SplashPageState();
}

class _SplashPageState extends State<SplashPage> {
  @override
  void initState() {
    super.initState();
    checkSession();
  }

  @override
  Widget build(BuildContext context) {
    return SafeArea(
      child: Scaffold(
        body: Center(
          child: Icon(
            Icons.movie,
            size: 100,
            color: Theme.of(context).primaryColor,
          ),
        ),
      ),
    );
  }

  Future<void> checkSession() async {
    await Future.delayed(const Duration(seconds: 2));
    var isLogged = await widget._repository.isLogged();

    isLogged ? Navigator.pushReplacementNamed(context, HomePage.name) : Navigator.pushReplacementNamed(context, LoginPage.name);
  }
}
