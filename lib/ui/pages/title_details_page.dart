import 'package:flutter/material.dart';
import 'package:intl/intl.dart';
import 'package:my_movies_list/data/exceptions/comment_by_other_user.dart';

import 'package:my_movies_list/data/models/title_detail_model.dart';
import 'package:my_movies_list/data/models/title_model.dart';
import 'package:my_movies_list/data/repositories/title_repository_interface.dart';
import 'package:my_movies_list/locator.dart';
import 'package:my_movies_list/ui/widgets/carousel.dart';
import 'package:my_movies_list/ui/widgets/customs/custom_text_form_field.dart';
import 'package:my_movies_list/ui/widgets/title_rate.dart';
import 'package:my_movies_list/ui/widgets/title_thumbnail.dart';

class TitleDetailsPage extends StatefulWidget {
  static const name = 'title-details-page';

  const TitleDetailsPage({Key? key}) : super(key: key);

  @override
  State<TitleDetailsPage> createState() => _TitleDetailsPageState();
}

class _TitleDetailsPageState extends State<TitleDetailsPage> {
  final _repository = getIt.get<TitleRepositoryInterface>();

  final _commentController = TextEditingController();

  @override
  Widget build(BuildContext context) {
    final arguments = (ModalRoute.of(context)!.settings.arguments as Map);
    final titleId = arguments['id'] as int;
    final isTvShow = arguments['is_tv_show'] as bool;

    return SafeArea(
      child: Scaffold(
        body: FutureBuilder<TitleDetailModel>(
          future: _repository.getTitleDetails(titleId, isTvShow: isTvShow),
          builder: (context, snapshot) {
            if (snapshot.connectionState == ConnectionState.waiting) {
              return const Center(
                child: CircularProgressIndicator(),
              );
            }

            if (snapshot.hasError) {
              return Center(
                child: Text(snapshot.error.toString()),
              );
            }

            var titleDetail = snapshot.data!;

            return Column(
              children: [
                Image.network(titleDetail.coverUrl),
                Expanded(
                  child: Padding(
                    padding: const EdgeInsets.all(12.0),
                    child: _buildDetails(context: context, titleDetail: titleDetail, isTvShow: isTvShow),
                  ),
                )
              ],
            );
          },
        ),
      ),
    );
  }

  Widget _buildDetails({required BuildContext context, required TitleDetailModel titleDetail, required bool isTvShow}) {
    return Column(
      crossAxisAlignment: CrossAxisAlignment.start,
      children: [
        Column(
          crossAxisAlignment: CrossAxisAlignment.start,
          children: [
            Text(
              titleDetail.name,
              style: const TextStyle(
                fontSize: 18.0,
                fontWeight: FontWeight.bold,
              ),
            ),
            if (titleDetail.releaseDate != null)
              Text(
                '(${DateFormat('dd/MM/yyyy').format(titleDetail.releaseDate!)})',
                style: const TextStyle(fontWeight: FontWeight.w700),
              ),
          ],
        ),
        const SizedBox(height: 10.0),
        Expanded(
          child: SingleChildScrollView(
            child: Column(
              crossAxisAlignment: CrossAxisAlignment.start,
              children: [
                const Text(
                  'Sinopse:',
                  style: TextStyle(fontWeight: FontWeight.w700),
                ),
                const SizedBox(height: 5.0),
                Text(
                  titleDetail.overview,
                  style: const TextStyle(fontSize: 15.0),
                ),
                if (titleDetail.runtime.isEmpty || titleDetail.runtime != '0')
                  Padding(
                    padding: const EdgeInsets.only(top: 10.0),
                    child: Text('Duração: ${titleDetail.runtime} min'),
                  ),
                const SizedBox(height: 10.0),
                _buildCarouselGenre(titleDetail.genres),
                const SizedBox(height: 5.0),
                _buildRate(titleDetail: titleDetail, isTvShow: isTvShow),
                _buildRecommendations(titleDetail: titleDetail, isTvShow: isTvShow),
                if (titleDetail.comments.isNotEmpty) _buildComments(context: context, titleDetail: titleDetail, isTvShow: isTvShow)
              ],
            ),
          ),
        ),
        const SizedBox(height: 10.0),
        CustomTextFormField(
          labelText: 'Adicione um comentário',
          controller: _commentController,
          suffix: IconButton(
            icon: const Icon(Icons.send_rounded),
            onPressed: () async {
              await _repository.saveComment(titleDetail.id, _commentController.text, isTvShow: isTvShow);
              _commentController.clear();
              setState(() {});
            },
          ),
        ),
      ],
    );
  }

  Widget _buildCarouselGenre(List<String> genres) {
    return Carousel(
      children: genres
          .map(
            (String e) => Padding(
              padding: const EdgeInsets.symmetric(horizontal: 2.5),
              child: Chip(
                label: Text(e),
              ),
            ),
          )
          .toList(),
    );
  }

  Widget _buildRate({required TitleDetailModel titleDetail, required bool isTvShow}) {
    return FutureBuilder<int>(
      future: _repository.getTitleRate(titleDetail.id, isTvShow: isTvShow),
      builder: (context, snapshot) {
        if (snapshot.connectionState == ConnectionState.waiting) {
          return const Center(
            child: CircularProgressIndicator(),
          );
        }

        return TitleRate(
          value: snapshot.hasData ? (snapshot.data! == 1) : null,
          onChanged: (bool value) async {
            await _repository.saveTitleRate(titleDetail.id, value ? 1 : -1);
            setState(() {});
          },
        );
      },
    );
  }

  Widget _buildRecommendations({required TitleDetailModel titleDetail, required bool isTvShow}) {
    return FutureBuilder<List<TitleModel>>(
      future: _repository.getTitleRecommendation(titleDetail.id, isTvShow: isTvShow),
      builder: (context, snapshot) {
        if (snapshot.connectionState == ConnectionState.waiting) {
          return const Center(
            child: CircularProgressIndicator(),
          );
        }

        if (!snapshot.hasData || snapshot.data!.isEmpty) {
          return const SizedBox();
        }

        var recommendations = snapshot.data!;
        return Padding(
          padding: const EdgeInsets.only(bottom: 10.0),
          child: Carousel(
            label: 'Recomendados',
            children: recommendations.map((e) => _buildCarouselRecommendation(context: context, title: e)).toList(),
          ),
        );
      },
    );
  }

  Widget _buildCarouselRecommendation({required TitleModel title, required BuildContext context}) {
    return GestureDetector(
      onTap: () => Navigator.pushNamed(context, TitleDetailsPage.name, arguments: {'id': title.id, 'is_tv_show': title.isTvShow}),
      child: TitleThumbnail(
        width: 100.0,
        height: 150.0,
        titleName: title.name,
        showBanner: false,
        urlThumbnail: title.posterUrl,
      ),
    );
  }

  Widget _buildComments({required BuildContext context, required TitleDetailModel titleDetail, required bool isTvShow}) {
    return Column(
      crossAxisAlignment: CrossAxisAlignment.stretch,
      children: [
        const Text(
          'Comentários',
          style: TextStyle(
            fontSize: 16.0,
            fontWeight: FontWeight.w700,
          ),
        ),
        ...titleDetail.comments
            .take(5)
            .map(
              (e) => e.text.isNotEmpty
                  ? Row(
                      mainAxisAlignment: MainAxisAlignment.spaceBetween,
                      children: [
                        Expanded(
                          child: ListTile(
                            title: Text(
                              e.text,
                              maxLines: 1,
                              overflow: TextOverflow.clip,
                            ),
                            subtitle: Text(
                              DateFormat('dd/MM/yyyy hh:mm:ss').format(e.date),
                            ),
                          ),
                        ),
                        IconButton(
                          onPressed: () async {
                            try {
                              await _repository.removeComment(titleDetail.id, e.id, isTvShow: isTvShow);
                              setState(() {});
                            } on CommentByOtherUserException {
                              ScaffoldMessenger.of(context).showSnackBar(const SnackBar(content: Text('Não é permitido excluir um comentário feito por outro usuário')));
                            } on Exception catch (e) {
                              ScaffoldMessenger.of(context).showSnackBar(SnackBar(content: Text(e.toString())));
                            }
                          },
                          icon: const Icon(Icons.delete_outline_outlined),
                        ),
                      ],
                    )
                  : const SizedBox(),
            )
            .toList(),
        if (titleDetail.comments.length > 5)
          TextButton(
            onPressed: () {},
            child: Text(
              'Ver todos os ${titleDetail.comments.length} comentários',
              textAlign: TextAlign.start,
            ),
          ),
      ],
    );
  }
}
