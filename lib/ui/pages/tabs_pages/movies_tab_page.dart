import 'package:flutter/material.dart';
import 'package:my_movies_list/data/models/genre_model.dart';
import 'package:my_movies_list/data/repositories/title_repository_interface.dart';
import 'package:my_movies_list/locator.dart';
import 'package:my_movies_list/ui/widgets/title_carousel.dart';

class MoviesTabPage extends StatelessWidget {
  final _repository = getIt.get<TitleRepositoryInterface>();

  MoviesTabPage({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return SingleChildScrollView(
      child: Column(
        children: [
          Padding(
            padding: const EdgeInsets.all(10.0),
            child: Column(
              children: <GenreModel>[
                GenreModel(id: 16, name: 'Animação'),
                GenreModel(id: 12, name: 'Aventura'),
                GenreModel(id: 27, name: 'Terror'),
              ]
                  .map((e) => Padding(
                        padding: const EdgeInsets.symmetric(vertical: 5.0),
                        child: TitleCarousel(
                          label: e.name,
                          future: _repository.getMovieList(params: {"genre": e.id}),
                        ),
                      ))
                  .toList(),
            ),
          ),
        ],
      ),
    );
  }
}
