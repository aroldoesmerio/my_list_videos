import 'package:flutter/material.dart';
import 'package:my_movies_list/data/repositories/title_repository_interface.dart';
import 'package:my_movies_list/locator.dart';
import 'package:my_movies_list/ui/widgets/title_carousel.dart';

class MainTabPage extends StatelessWidget {
  final _repository = getIt.get<TitleRepositoryInterface>();

  MainTabPage({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return SingleChildScrollView(
      child: Column(
        children: [
          Padding(
            padding: const EdgeInsets.all(10.0),
            child: Column(children: _buildList()),
          ),
        ],
      ),
    );
  }

  List<Widget> _buildList() {
    List<Widget> _list = [];

    return _list
      ..add(_buildUpcomingMovieList())
      ..add(_buildPopularMovieList())
      ..add(_buildPopularTvList());
  }

  Widget _buildPopularMovieList() {
    return Padding(
      padding: const EdgeInsets.symmetric(vertical: 5.0),
      child: TitleCarousel(
        label: 'Filmes populares',
        future: _repository.getPopularMovieList(),
      ),
    );
  }

  Widget _buildPopularTvList() {
    return Padding(
      padding: const EdgeInsets.symmetric(vertical: 5.0),
      child: TitleCarousel(
        label: 'Séries populares',
        future: _repository.getPopularTvList(),
      ),
    );
  }

  Widget _buildUpcomingMovieList() {
    return Padding(
      padding: const EdgeInsets.symmetric(vertical: 5.0),
      child: TitleCarousel(
        label: 'Filmes recentes',
        future: _repository.getUpcomingMovieList(),
      ),
    );
  }
}
