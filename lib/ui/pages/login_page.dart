import 'package:flutter/material.dart';
import 'package:my_movies_list/data/exceptions/user_not_found.dart';
import 'package:my_movies_list/data/repositories/user_repository_interface.dart';
import 'package:my_movies_list/locator.dart';
import 'package:my_movies_list/ui/pages/home_page.dart';
import 'package:my_movies_list/ui/pages/register_account_page.dart';
import 'package:my_movies_list/ui/widgets/customs/custom_loading_elevated_button.dart';
import 'package:my_movies_list/ui/widgets/customs/custom_text_form_field.dart';

class LoginPage extends StatefulWidget {
  static const name = 'login-page';

  final _repository = getIt.get<UserRepositoryInterface>();

  LoginPage({Key? key}) : super(key: key);

  @override
  State<LoginPage> createState() => _LoginPageState();
}

class _LoginPageState extends State<LoginPage> {
  final emailController = TextEditingController();
  final passwordController = TextEditingController();

  String loginError = '';
  bool processingLogin = false;

  @override
  Widget build(BuildContext context) {
    return SafeArea(
      child: Scaffold(
        body: Padding(
          padding: const EdgeInsets.symmetric(horizontal: 10),
          child: Center(
            child: SingleChildScrollView(
              child: Column(
                crossAxisAlignment: CrossAxisAlignment.stretch,
                children: [
                  Text(
                    'Informe suas credenciais para começar',
                    textAlign: TextAlign.center,
                    style: TextStyle(
                      color: Theme.of(context).primaryColor,
                      fontSize: 20.0,
                      fontWeight: FontWeight.w300,
                    ),
                  ),
                  const SizedBox(height: 25.0),
                  CustomTextFormField(
                    labelText: 'Email',
                    controller: emailController,
                    keyboardType: TextInputType.emailAddress,
                  ),
                  const SizedBox(height: 20.0),
                  CustomTextFormField(
                    labelText: 'Senha',
                    controller: passwordController,
                    obscureText: true,
                  ),
                  const SizedBox(height: 25.0),
                  Visibility(
                    visible: loginError.isNotEmpty,
                    child: Text(
                      loginError,
                      style: const TextStyle(color: Colors.red),
                    ),
                  ),
                  CustomLoadingElevatedButton(
                    child: const Text('Entrar'),
                    isLoading: processingLogin,
                    onPressed: login,
                  ),
                  const SizedBox(height: 15.0),
                  Text(
                    'OU',
                    style: TextStyle(
                      color: Theme.of(context).primaryColor,
                      fontSize: 12.0,
                    ),
                    textAlign: TextAlign.center,
                  ),
                  TextButton(
                    child: const Text('Criar minha conta'),
                    onPressed: () => Navigator.pushNamed(context, RegisterAccountPage.name),
                  ),
                ],
              ),
            ),
          ),
        ),
      ),
    );
  }

  Future<void> login() async {
    setState(() {
      loginError = '';
      processingLogin = true;
    });

    try {
      await widget._repository.login(emailController.text, passwordController.text);
      Navigator.pushReplacementNamed(context, HomePage.name);
    } on UserNotFoundException {
      setState(() {
        loginError = 'Usuário não encontrado';
        processingLogin = false;
      });
    } on Exception catch (e) {
      setState(() {
        loginError = e.toString();
        processingLogin = false;
      });
    }
  }
}
