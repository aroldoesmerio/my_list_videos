import 'package:flutter/material.dart';
import 'package:my_movies_list/data/models/title_rated_model.dart';
import 'package:my_movies_list/data/repositories/title_repository_interface.dart';
import 'package:my_movies_list/locator.dart';

import 'package:my_movies_list/ui/widgets/customs/custom_image_network.dart';
import 'package:my_movies_list/ui/widgets/title_rate.dart';

class RatedTitleListPage extends StatefulWidget {
  static const name = 'rated-title-page';

  const RatedTitleListPage({Key? key}) : super(key: key);

  @override
  State<RatedTitleListPage> createState() => _RatedTitleListPageState();
}

class _RatedTitleListPageState extends State<RatedTitleListPage> {
  final _repository = getIt.get<TitleRepositoryInterface>();

  @override
  Widget build(BuildContext context) {
    final args = (ModalRoute.of(context)!.settings.arguments as Map? ?? {});
    final String? userId = args['user_id'];
    final String? userName = args['user_name'];

    return SafeArea(
      child: Scaffold(
        appBar: AppBar(
          title: Text(userName ?? 'Minhas avaliações'),
        ),
        body: Padding(
          padding: const EdgeInsets.all(10),
          child: SingleChildScrollView(
            child: FutureBuilder<List<TitleRatedModel>>(
              future: _repository.getUserRatedTitleList(userId),
              builder: (context, snapshot) {
                if (snapshot.connectionState == ConnectionState.waiting) {
                  return const Center(
                    child: CircularProgressIndicator(),
                  );
                }

                if (snapshot.hasError) {
                  return const Center(
                    child: Text('Falha ao carregar os títulos avaliados'),
                  );
                }

                return Column(
                  children: snapshot.data!.map((e) => _buildTitleCard(context: context, title: e, canRate: userId == null)).toList(),
                );
              },
            ),
          ),
        ),
      ),
    );
  }

  Widget _buildTitleCard({required TitleRatedModel title, required BuildContext context, bool canRate = false}) {
    return Padding(
      padding: const EdgeInsets.only(bottom: 8.0),
      child: Row(
        mainAxisAlignment: MainAxisAlignment.spaceBetween,
        children: [
          Expanded(
            child: Row(
              mainAxisAlignment: MainAxisAlignment.start,
              children: [
                Container(
                  height: 90.0,
                  decoration: BoxDecoration(
                    color: Theme.of(context).primaryColor,
                    borderRadius: BorderRadius.circular(5),
                  ),
                  child: Padding(
                    padding: const EdgeInsets.all(2.5),
                    child: CustomImageNetWork(
                      urlImage: title.posterUrl,
                      height: 80.0,
                    ),
                  ),
                ),
                const SizedBox(width: 10.0),
                Expanded(
                  child: Text(
                    title.name,
                    style: const TextStyle(
                      fontSize: 16.0,
                      fontWeight: FontWeight.bold,
                    ),
                  ),
                ),
              ],
            ),
          ),
          TitleRate(
            value: title.rate != null ? (title.rate == 1) : null,
            onChanged: canRate
                ? (bool value) async {
                    await _repository.saveTitleRate(title.id, value ? 1 : -1);
                    setState(() {});
                  }
                : null,
          ),
        ],
      ),
    );
  }
}
