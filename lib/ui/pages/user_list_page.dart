import 'package:flutter/material.dart';
import 'package:my_movies_list/data/models/user_model.dart';
import 'package:my_movies_list/data/repositories/user_repository_interface.dart';
import 'package:my_movies_list/locator.dart';
import 'package:my_movies_list/ui/pages/rated_title_list_page.dart';

class UserListPage extends StatelessWidget {
  static const name = 'user-list-page';

  final _repository = getIt.get<UserRepositoryInterface>();

  UserListPage({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return SafeArea(
      child: Scaffold(
        appBar: AppBar(
          title: const Text('Lista de usuários'),
        ),
        body: Padding(
          padding: const EdgeInsets.all(10.0),
          child: SingleChildScrollView(
            child: FutureBuilder<List<UserModel>>(
                future: _repository.listUsers(),
                builder: (context, snapshot) {
                  if (snapshot.connectionState == ConnectionState.waiting) {
                    return const Center(
                      child: CircularProgressIndicator(),
                    );
                  }

                  if (snapshot.hasError) {
                    return const Center(
                      child: Text('Falha ao carregar os usuários'),
                    );
                  }

                  return Column(
                    children: snapshot.data!.map((e) => _buildUsers(context: context, user: e)).toList(),
                  );
                }),
          ),
        ),
      ),
    );
  }

  Widget _buildUsers({required UserModel user, required BuildContext context}) {
    return Padding(
      padding: const EdgeInsets.symmetric(vertical: 5.0),
      child: GestureDetector(
        onTap: () => Navigator.pushNamed(context, RatedTitleListPage.name, arguments: {'user_id': user.id, 'user_name': user.name}),
        child: Row(
          mainAxisAlignment: MainAxisAlignment.start,
          children: [
            CircleAvatar(
              child: Text(
                user.name[0].toUpperCase(),
                style: const TextStyle(
                  fontSize: 18.0,
                  fontWeight: FontWeight.bold,
                ),
              ),
            ),
            const SizedBox(width: 10.0),
            Expanded(
              child: Text(
                user.name,
                style: const TextStyle(fontSize: 16.0),
              ),
            ),
          ],
        ),
      ),
    );
  }
}
