import 'package:flutter/material.dart';
import 'package:font_awesome_flutter/font_awesome_flutter.dart';
import 'package:my_movies_list/data/exceptions/user_already_exists.dart';
import 'package:my_movies_list/data/repositories/user_repository_interface.dart';
import 'package:my_movies_list/locator.dart';
import 'package:my_movies_list/ui/pages/home_page.dart';
import 'package:my_movies_list/ui/widgets/customs/custom_loading_elevated_button.dart';
import 'package:my_movies_list/ui/widgets/customs/custom_text_form_field.dart';

class RegisterAccountPage extends StatefulWidget {
  static const name = 'register-account-page';

  final _repository = getIt.get<UserRepositoryInterface>();

  RegisterAccountPage({Key? key}) : super(key: key);

  @override
  State<RegisterAccountPage> createState() => _RegisterAccountPageState();
}

class _RegisterAccountPageState extends State<RegisterAccountPage> {
  final nameController = TextEditingController();
  final emailController = TextEditingController();
  final passwordController = TextEditingController();

  String registerError = '';
  bool processingRegister = false;

  @override
  Widget build(BuildContext context) {
    return SafeArea(
      child: Scaffold(
        body: Padding(
          padding: const EdgeInsets.symmetric(horizontal: 10.0),
          child: Center(
            child: SingleChildScrollView(
              scrollDirection: Axis.vertical,
              child: Column(
                crossAxisAlignment: CrossAxisAlignment.stretch,
                children: [
                  Center(
                    child: FaIcon(
                      FontAwesomeIcons.userPlus,
                      size: 45,
                      color: Theme.of(context).primaryColor,
                    ),
                  ),
                  const SizedBox(height: 15.0),
                  Text(
                    'Crie sua conta',
                    style: TextStyle(
                      color: Theme.of(context).primaryColor,
                      fontSize: 20.0,
                      fontWeight: FontWeight.w300,
                    ),
                    textAlign: TextAlign.center,
                  ),
                  const SizedBox(height: 25.0),
                  CustomTextFormField(
                    labelText: 'Nome',
                    controller: nameController,
                  ),
                  const SizedBox(height: 15.0),
                  CustomTextFormField(
                    labelText: 'Email',
                    controller: emailController,
                    keyboardType: TextInputType.emailAddress,
                  ),
                  const SizedBox(height: 15.0),
                  CustomTextFormField(
                    labelText: 'Senha',
                    controller: passwordController,
                    obscureText: true,
                  ),
                  const SizedBox(height: 25.0),
                  Visibility(
                    child: Text(
                      registerError,
                      style: const TextStyle(color: Colors.red),
                    ),
                  ),
                  CustomLoadingElevatedButton(
                    child: const Text('Entrar'),
                    isLoading: processingRegister,
                    onPressed: register,
                  ),
                  TextButton(
                    child: const Text('Voltar'),
                    onPressed: () => Navigator.pop(context),
                  ),
                ],
              ),
            ),
          ),
        ),
      ),
    );
  }

  Future<void> register() async {
    setState(() {
      registerError = '';
      processingRegister = true;
    });

    try {
      await widget._repository.register(emailController.text, passwordController.text, nameController.text);
      Navigator.pushReplacementNamed(context, HomePage.name);
    } on UserAlreadyExistsException {
      setState(() {
        registerError = 'Usuário já existente';
        processingRegister = false;
      });
    } on Exception catch (e) {
      setState(() {
        registerError = e.toString();
        processingRegister = false;
      });
    }
  }
}
