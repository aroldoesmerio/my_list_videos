import 'package:flutter/material.dart';

import 'package:my_movies_list/ui/widgets/customs/custom_image_network.dart';

class TitleThumbnail extends StatelessWidget {
  final String urlThumbnail;
  final String titleName;
  final double width;
  final double height;
  final bool showBanner;

  const TitleThumbnail({
    Key? key,
    required this.urlThumbnail,
    required this.titleName,
    required this.width,
    required this.height,
    this.showBanner = true,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Padding(
      padding: const EdgeInsets.symmetric(horizontal: 2.5),
      child: Container(
        decoration: BoxDecoration(
          color: Theme.of(context).primaryColor,
          borderRadius: BorderRadius.circular(5),
        ),
        width: width,
        height: height,
        child: Padding(
          padding: const EdgeInsets.all(5.0),
          child: Column(
            children: [
              Expanded(
                child: CustomImageNetWork(
                  height: height,
                  urlImage: urlThumbnail,
                  imageErrorText: titleName,
                ),
              ),
              if (showBanner)
                Column(
                  mainAxisAlignment: MainAxisAlignment.center,
                  children: [
                    const SizedBox(height: 5),
                    Text(
                      titleName,
                      style: const TextStyle(fontSize: 12.0, fontWeight: FontWeight.w400),
                      maxLines: 1,
                      overflow: TextOverflow.ellipsis,
                    ),
                  ],
                ),
            ],
          ),
        ),
      ),
    );
  }
}
